require 'CSV'
require 'pry'

res = {}

f = File.new(ARGV[0])

f.read().each_char do |sym|
	if res.key?(sym)
		res[sym] += 1
	else
		res[sym] = 1
	end
end

CSV.open(ARGV[1], "wb") do |csv|
	res.sort_by { |key, value| -value }
	   .each do |value|
	   		p "#{value[0]}: #{value[1]}" 
			csv << [value[0], value[1]]
		end
end
f.close()