using System;
using System.Collections.Generic;
using System.IO;

class Node : IComparable
{
    public int a;
    public byte c;
    public Node left, right;

    public Node() { left = right = null; }
    public Node(Node L, Node R)
    {
        left = L;
        right = R;
        a = L.a + R.a;
    }

    public int CompareTo(object obj)
    {
        if (obj == null) return 1;

        Node temp = obj as Node;
        if (temp != null)
            return this.a.CompareTo(temp.a);
        else
            throw new ArgumentException("Object is not Node");
    }
}

class CreateTable
{
    public List<int> code;
    public Dictionary<byte, List<int>> table;

    public CreateTable(Node root)
    {
        code = new List<int>();
        table = new Dictionary<byte, List<int>>();
        BuildTable(root);
    }

    void BuildTable(Node root)
    {
        if (root.left != null)
        {
            code.Add(0);
            BuildTable(root.left);
        }
        if (root.right != null)
        {
            code.Add(1);
            BuildTable(root.right);
        }
        if (root.right == null && root.left == null)
        {
            List<int> temp = new List<int>(code);
            table.Add(root.c, temp);
        }

        if (code.Count > 0)
            code.RemoveAt(code.Count - 1);
    }
}

class Program4
{
    static void Main(string[] args)
    {
        int signature = 991828;

        string path = Environment.CurrentDirectory;
        string PathOfOriginal = Path.Combine( path, args[0]);
        string ZipPath = Path.Combine ( path, args[1] );
        string UnZipPath = Path.Combine (path, args[1]);

        if (args[2] == "reduce")
        {
            byte[] buffer = File.ReadAllBytes(PathOfOriginal);

            // считаем одинаковых элементов в массиве
            Dictionary<byte, int> m = new Dictionary<byte, int>();
            for (int i = 0; i < buffer.Length; ++i)
            {
                byte c = buffer[i];
                if (m.ContainsKey(c))
                    ++m[c];
                else
                    m.Add(c, 1);
            }

            //  записываем начальные узлы в список list
            ICollection<byte> keys = m.Keys;
            List<Node> parts = new List<Node>();
            foreach (byte ch in keys)
            {
                Node p = new Node();
                p.c = ch;
                p.a = m[ch];
                parts.Add(p);
            }

            // create the tree
            while (parts.Count != 1)
            {
                parts.Sort();

                Node SonL = parts[0];
                parts.RemoveAt(0);
                Node SonR = parts[0];
                parts.RemoveAt(0);

                Node parent = new Node(SonL, SonR);
                parts.Add(parent);
            }

            Node root = parts[0];
            // создаем пары 'символ-код':	
            CreateTable ct = new CreateTable(root);
            Dictionary<byte, List<int>> table = ct.table;

            // создаём вектор кодировки и записываем в файл
            int quantity = 0; // количество нулей и единиц, т.е. размер вектора
            List<int> res = new List<int>();
            using (BinaryWriter writer = new BinaryWriter(File.Open(ZipPath, FileMode.Create)))
            {
                //запись таблицы

                // write signature
                writer.Write(signature);

                writer.Write(m.Count); // количество пар

                foreach (byte ch in keys) // записываем пары
                {
                    writer.Write(ch);
                    writer.Write(m[ch]);
                }

                // записиь кодируемого вектера
                for (int i = 0; i < buffer.Length; ++i)
                {
                    List<int> vec = table[buffer[i]];
                    for (int k = 0; k < vec.Count; ++k)
                    {
                        res.Add(vec[k]);
                    }
                }
                quantity = res.Count;
                writer.Write(quantity);// запишем колиство нулей и единиц

                int count = 0;
                int buf = 0;
                for (int i = 0; i < quantity; ++i)
                {
                    //запишем в файл
                    buf = buf | (res[i] << (7 - count));
                    ++count;
                    if (count == 8) { byte temp = (byte)buf; writer.Write(temp); count = 0; buf = 0; }
                    //Console.Write(res[res.Count - 1]); // вывод вектора битов
                }
                if (buf != 0) writer.Write((byte)buf);
            }
            Console.WriteLine("Success!");
        }

        else if (args[2] == "unzip")
        {
            using (BinaryReader reader = new BinaryReader(File.Open(PathOfOriginal, FileMode.Open)))
            {
                int writedSignature = reader.ReadInt32();

                if (writedSignature != signature) {
                    Console.WriteLine("Signature is not compatible!");
                    return;
                }

                // читаем словарь из бинарника
                Dictionary<byte, int> ReadedDictionary = new Dictionary<byte, int>();
                int quantity = reader.ReadInt32();

                for (int i = 0; i < quantity; ++i)
                {
                    ReadedDictionary.Add(reader.ReadByte(), reader.ReadInt32());
                }
                //  записываем начальные узлы в список list
                ICollection<byte> keys = ReadedDictionary.Keys;
                List<Node> parts = new List<Node>();
                foreach (byte ch in keys)
                {
                    Node p = new Node();
                    p.c = ch;
                    p.a = ReadedDictionary[ch];
                    parts.Add(p);
                }

                // create the tree
                while (parts.Count != 1)
                {
                    parts.Sort();

                    Node SonL = parts[0];
                    parts.RemoveAt(0);
                    Node SonR = parts[0];
                    parts.RemoveAt(0);

                    Node parent = new Node(SonL, SonR);
                    parts.Add(parent);
                }

                Node root = parts[0]; // вершина дерева


                int Number = reader.ReadInt32(); // кол-во элемнтов, которые нужно считать
                int QuantityOfBytes = Number / 8; // кол-во считанных байтов
                if (Number % 8 != 0) ++QuantityOfBytes;
                int counterOfBits = 0;

                using (BinaryWriter writer2 = new BinaryWriter(File.Open(UnZipPath, FileMode.Create)))
                {
                    Node current = root;
                    for (int i = 0; i < QuantityOfBytes; ++i)
                    {
                        int newbyte = reader.ReadByte();
                        //Console.WriteLine("Byte: " + newbyte);
                        for (int counter = 0; counter < 8; ++counter)
                        {
                            if (counterOfBits == Number) break;
                            int b = newbyte & 1 << (7 - counter);
                            if (b == 0) current = current.left;
                            else if (b != 0) current = current.right;
                            if (current.left == null && current.right == null)
                            {
                                //Console.Write((char)current.c);
                                writer2.Write(current.c); // выведем разархивированный файл
                                current = root;
                            }
                            ++counterOfBits;
                        }
                    }
                }
            }
            Console.WriteLine("Success!");
        }
    }
}
